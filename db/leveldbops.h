#ifndef LevelDBOps_H
#define LevelDBOps_H

#include <string>

#include "leveldb/db.h"

class LevelDBOps
{
public:
	LevelDBOps(std::__cxx11::string name);
	~LevelDBOps();
	int put(std::string key, std::string value);
	std::__cxx11::string read(std::string key);
protected:
	leveldb::DB *db;
	leveldb::Options options;
	leveldb::ReadOptions readOptions;
	leveldb::WriteOptions writeOptions;
	bool isOk;
};

#endif // LevelDBOps_H
