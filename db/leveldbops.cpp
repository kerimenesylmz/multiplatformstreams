#include "leveldbops.h"

#include <iostream>
#include <sstream>
#include "loguru.hpp"

LevelDBOps::LevelDBOps(std::string name)
{
	isOk = true;
	options.create_if_missing = true;

	leveldb::Status status = leveldb::DB::Open(options, name, &db);
	if (!status.ok()) {
		LOG_F(INFO, "DB cannot open. %s", name.data());
		LOG_F(INFO, "Error details %s", status.ToString().data());
		isOk = false;
	}
}

LevelDBOps::~LevelDBOps()
{
	if (db)
		delete db;
}

int LevelDBOps::put(std::string key, std::string value)
{
	leveldb::Status status = db->Put(writeOptions, key, value);
	if (!status.ok()) {
		LOG_F(INFO, "Cannot write to DB: '%s'", status.ToString().data());
		return -1;
	}
	return 0;
}

std::string LevelDBOps::read(std::string key)
{
	std::string value;
	leveldb::Status status = db->Get(readOptions, key, &value);
	if (!status.ok()) {
		LOG_F(INFO, "Cannot read from DB: '%s'", status.ToString().data());
		return "";
	}
	return value;
}

