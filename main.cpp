#include "stdio.h"
#include "debug.h"
#include "loguru.hpp"

#include <unistd.h>
#include <m11/m11common.h>
#include <m11/h264element.h>
#include <m11/h265element.h>
#include <m11/jpegelement.h>
#include <m11/capturepipeline.h>
#include <m11/snapshotelement.h>
#include <m11/yuvelement.h>
#include <m11/camsio/camcommunication.h>

#include <m11/camsio/registercontrol.h>
#include <m11/camsio/camsiogrpcserver.h>

#include <jsonparser.h>
#include <elementsettings.h>
#include <list>
#include <xarina/capturesettings.h>
#include <xarina/testvideoinput.h>

#include <omxvideocontrol.h>
#include <omxcaptureelement.h>
#include "grpc/grpcstreamerserver.h"


int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	loguru::g_stderr_verbosity = 3;
	char *var = getenv("DEBUG");
	if (var)
		loguru::g_stderr_verbosity = std::stoi(var);
#if 0
	OmxVideoControl *omx = new OmxVideoControl();
	omx->start();
	std::shared_ptr<OmxCaptureElement> omx = std::make_shared<OmxCaptureElement>();
	omx->start();
	OmxCaptureElement *omx = new OmxCaptureElement();
	omx->start();
	CapturePipeline *capPipe = new CapturePipeline();
	capPipe->addPipe(omx);
	capPipe->start();
	while (1) {
		usleep(1000);
	}
	return 0;
	
	CaptureSettings *cap = new CaptureSettings();
	cap->setInputSource(1);
	gWarn("%d", cap->getInputSource());

	TestVideoInput *testIn = new TestVideoInput();
	testIn->startProcess();
#endif

        M11Common m11;
        m11.videoInit();
        m11.paramInit();
        m11.captureInit();

	std::list<CapturePipeline*> capList;

	std::shared_ptr<M11Element> h264 = std::make_shared<H264Element>();
	std::shared_ptr<M11Element> h264_Rec = std::make_shared<H264Element>();
	std::shared_ptr<M11Element> h265 = std::make_shared<H265Element>();
	std::shared_ptr<M11Element> snap = std::make_shared<SnapshotElement>();
	std::shared_ptr<M11Element> jpeg = std::make_shared<JpegElement>();
	std::shared_ptr<M11Element> yuv = std::make_shared<YUVElement>();

	ElementSettings *elSets = new ElementSettings();
	elSets->adjustEl(h265, "hevc");
	elSets->adjustEl(snap, "snapshot");
	elSets->adjustEl(h264_Rec, "h264");

	JsonParser *stream = new JsonParser("stream.json");
	if (stream->getBool("h264.enable")) {
		CapturePipeline *cap = new CapturePipeline();
		cap->startRtspStream(h264, stream->getInt("h264.port"), "stream1");
		capList.push_back(cap);
	}

	if (stream->getBool("grpc.enable")) {
		CapturePipeline *cap = new CapturePipeline();
		if (stream->getBool("h264.enable"))
			cap->startGrpcStream(h264);
		else if (stream->getBool("hevc.enable"))
                        cap->startGrpcStream(hevc);
		capList.push_back(cap);
		GrpcStreamerServer streamer(cap->getIface());
		streamer.startAndWait();
	}
	
	if (stream->getBool("hevc.enable")) {
		CapturePipeline *cap = new CapturePipeline();
		cap->startSimpleTestStream(h265);
		capList.push_back(cap);
	}

	if (stream->getBool("mjpeg.enable")) {
		CapturePipeline *capSnap = new CapturePipeline();
		capSnap->startMjpegStream(snap, stream->getInt("mjpeg.port"));
		capList.push_back(capSnap);
	}

	if (stream->getBool("sensor_grpc.enable")) {
		CamsioGrpcServer *grpc = new CamsioGrpcServer();
		std::string ep = "0.0.0.0:" + std::to_string(stream->getInt("sensor_grpc.port"));
		gWarn("Starting server with %s", ep.data());
		grpc->startWait(ep);
	}

	if (stream->getBool("record.enable")) {
		CapturePipeline *capRec = new CapturePipeline();
		capRec->startRecordPipeline(h264_Rec);
		capList.push_back(capRec);
	}


	while(1) {
		usleep(1000);
	}
	return 0;
}
