#include "m11common.h"
#include "debug.h"

#include <errno.h>
#include <cstring>

M11Common::M11Common()
{

}

int M11Common::videoInit()
{
	if (sc_video_init()) {
		gWarn("Video library unitialized.");
		return -ELIBBAD;
	}
	return 0;
}

int M11Common::paramInit()
{
	if (sc_param_init()) {
		gWarn("Param library uninitialized.");
		return -ELIBBAD;
	}
	return 0;
}

int M11Common::videoExit()
{
	if (sc_video_exit()) {
		gWarn("Error in, Exiting video library");
		return -ELIBBAD;
	}
	return 0;
}

int M11Common::paramExit()
{
	if (sc_param_exit()) {
		gWarn("Error in, Exiting param library");
		return -ELIBBAD;
	}
	return 0;
}

int M11Common::captureInit()
{
	if (sc_cap_open()) {
		gWarn("Capture library uninitialized");
		return -ELIBBAD;
	}
	return 0;
}

int M11Common::captureExit()
{
	if (sc_cap_close()) {
		gWarn("Error in, Exiting capture library");
		return -ELIBBAD;
	}
	return 0;
}

