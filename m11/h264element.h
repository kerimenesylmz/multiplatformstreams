#ifndef H264ELEMENT_H
#define H264ELEMENT_H

#include <m11/m11element.h>

class H264Element : public M11Element
{
public:
	H264Element();
	int start();
	int changeParam();
protected:
	uint id;
	SC_VENC_INI_H264 *ini;
};

#endif // H264ELEMENT_H
