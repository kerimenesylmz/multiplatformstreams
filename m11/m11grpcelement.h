#ifndef M11GRPCELEMENT_H
#define M11GRPCELEMENT_H

#include "pipeline/pelement.h"
#include "grpc/nvr.grpc.pb.h"

class StreamerIface
{
public:
	::grpc::ServerReaderWriter<vms::StreamFrame, vms::CameraStreamQ> *writer;
};

class M11GrpcElement : public PElement
{
public:
	M11GrpcElement();
	StreamerIface *iface;
protected:
	int processBuffer(int ch, PBuffer &buf) override;

};

#endif // M11GRPCELEMENT_H
