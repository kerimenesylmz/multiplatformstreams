#include "m11grpcelement.h"
#include "debug.h"

#include "grpc/nvr.grpc.pb.h"

using namespace std;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;
#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/channel.h>
#include <grpc++/create_channel.h>
#include <grpc++/client_context.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/credentials.h>
#include <grpc++/security/server_credentials.h>

M11GrpcElement::M11GrpcElement()
{
	iface = new StreamerIface;
	iface->writer = NULL;
}

int M11GrpcElement::processBuffer(int ch, PBuffer &buf)
{
	(void)ch;
	if (iface->writer) {
		vms::StreamFrame sframe;
		vms::StreamBuffer *sbuf = sframe.add_data();
		sbuf->set_data(buf.data(), buf.size());
		bool success = iface->writer->Write(sframe);
		if (!success) {
			gWarn("FAILED");
			// TODO: delete writer object.
			iface->writer = NULL;
		}
	}
	return 0;
}
