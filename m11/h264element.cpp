#include "h264element.h"

#include <errno.h>
#include <debug.h>

H264Element::H264Element()
	: M11Element("h264")
{
	id = 0;
	ini = &param.codec.h264.ini;
	ini->threadId = 0;
	ini->bufferFrameNum = 7;

	setThreadID(ini->threadId);
	// TODO: why change it ? 
	// inputSource value can get from ISP.
	setInputSource(1);
	setReleaseMode(ini->releaseMode);
}

int H264Element::start()
{
	printParams();
	SC_VENC_INI_H264_INFO h264Info;
	int ret = sc_venc_open_h264_session(ini, &id, &h264Info);
	gWarn("Error %d, id %d, %d", ret, id, errno);
	if (ret < 0)
		return -ENOSTR;
	setStreamID(id);
	return M11Element::start();
}

int H264Element::changeParam()
{
	if (!id)
		return 0;
	SC_VENC_CHG_H264PRM prm;
	prm.outImage = ini->outImage;
	prm.codecParam = ini->codecParam;
	int err = sc_venc_change_h264_param(id, &prm);
	gWarn("Error code is %d", err);
	return M11Element::changeParam();
}
