#ifndef H265ELEMENT_H
#define H265ELEMENT_H

#include <m11/m11element.h>

class H265Element : public M11Element
{
public:
	H265Element();
	int start();
protected:
	SC_VENC_INI_HEVC *ini;
};

#endif // H265ELEMENT_H
