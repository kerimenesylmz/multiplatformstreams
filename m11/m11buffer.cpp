#include "m11buffer.h"

#include <memory>
#include "debug.h"

static void deletePacket(SC_VENC_ENC_DATA *ves, int id)
{
	if (ves->shm.pStreamAdr != NULL)
		sc_venc_release_stream_data(id, &ves->shm);
	delete ves;
}

M11Buffer::M11Buffer()
{
	std::shared_ptr<M11Buffer::M11Data> m11buf = std::make_shared<M11Buffer::M11Data>();
	d = m11buf;
	d->pars.type = BUFFER_M11;
}

M11Buffer::M11Buffer(SC_VENC_ENC_DATA *ves, int eID)
{
	std::shared_ptr<M11Buffer::M11Data> m11buf = std::make_shared<M11Buffer::M11Data>();
	m11buf->ves = ves;
	m11buf->id = eID;
	d = m11buf;
	d->pars.type = BUFFER_M11;
}

M11Buffer::~M11Buffer()
{

}


M11Buffer::M11Data::M11Data()
	: PBuffer::Data()
{
	ves = NULL;
}

M11Buffer::M11Data::~M11Data()
{
	if (ves) {
		gLog("Delete packet size: %d", ves->shm.streamSize);
		deletePacket(ves, id);
		ves = NULL;
	}
}

size_t M11Buffer::M11Data::getLen() const
{
	return ves->shm.streamSize;
}

unsigned char *M11Buffer::M11Data::getData()
{
	return (unsigned char*)ves->shm.pStreamAdr;
}
