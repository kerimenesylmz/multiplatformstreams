#include "camerabasiccontrol.h"

#include "registercontrol.h"
#include "debug.h"
#include "errno.h"
#include <unistd.h>

CameraBasicControl::CameraBasicControl()
{
	reg = new RegisterControl();
}

int CameraBasicControl::setWhiteBalanceMode(const camsio::WhiteBalanceQ *wbq)
{
	reg->getRegisterValue("AWBMODE");
	gWarn("lala %d", wbq->modes());
	return reg->writeReg(wbq->modes());
}

int CameraBasicControl::getWhiteBalanceMode(camsio::WhiteBalanceQ *info)
{
	reg->getRegisterValue("AWBMODE");
	uint data = 0;
	int ret = reg->readReg(data);
	info->set_modes(static_cast<camsio::WhiteBalanceQ_WhiteBalanceModes>(data));
	return ret;
}

int CameraBasicControl::setATWMode(camsio::ATWModeQ_ATWModes mode)
{
	if (mode == camsio::ATWModeQ_ATWModes_INDOOR) {
		reg->getRegisterValue("INOUT_MODE");
		reg->writeReg(0x00);
		reg->getRegisterValue("OUTDR_F_OUT_SW_ATW");
		reg->writeReg(0x02);
	} else if (mode == camsio::ATWModeQ_ATWModes_OUTDOOR) {
		reg->getRegisterValue("INOUT_MODE");
		reg->writeReg(0x01);
		reg->getRegisterValue("OUTDR_F_OUT_SW_ATW");
		reg->writeReg(0x01);
	} else if (mode == camsio::ATWModeQ_ATWModes_AUTO) {
		reg->getRegisterValue("INOUT_MODE");
		reg->writeReg(0x02);
		reg->getRegisterValue("OUTDR_F_OUT_SW_ATW");
		reg->writeReg(0x00);
	} else
		return -ENODATA;
	return 0;
}

int CameraBasicControl::setTemperatureValue(int value)
{
	if (value < 1500 || value > 15000)
		return -ENODATA;
	reg->getRegisterValue("MWBPOS");
	uint data = (value * 0x3f) / 15000;
	return reg->writeReg(data);
}

int CameraBasicControl::setUserModeWB(camsio::UserModeQ_UserModes mode)
{
	reg->getRegisterValue("WB_USRSCOPE_EN");
	uint data = 0x00;
	reg->readReg(data);
	data = data & 0xfd;
	reg->writeReg(data);
	reg->getRegisterValue("AWBUSER_NO");
	return reg->writeReg(mode);
}

int CameraBasicControl::setFullMWBMode(const camsio::FullMWBQ *mwbq)
{
	reg->getRegisterValue("FULLMWBINOUTSEL");
	reg->writeReg(mwbq->mode());

	reg->getRegisterValue("FULLMWBGAIN_R");
	reg->writeReg(mwbq->full_mwb_gain_r());
	reg->getRegisterValue("FULLMWBGAIN_GR");
	reg->writeReg(mwbq->full_mwb_gain_gr());
	reg->getRegisterValue("FULLMWBGAIN_GB");
	reg->writeReg(mwbq->full_mwb_gain_gb());
	reg->getRegisterValue("FULLMWBGAIN_B");
	reg->writeReg(mwbq->full_mwb_gain_b());
	return 0;
}

int CameraBasicControl::startWhiteBalancePreAdj()
{
	reg->getRegisterValue("ADJMODE");
	reg->writeReg(0x01); // open mode

	reg->getRegisterValue("AWB_ADJ_SUB_MODE");
	uint data = 0x00;
	reg->readReg(data);
	int cnt = 0;
	while (1) {
		gWarn("~~~~ cnt %d,,,, %d", cnt, data);
		reg->readReg(data);
		switch (data) {
		case 0x00:
			data = 0x01;
			break;
		case 0x02:
			data = 0x03;
			break;
		case 0x04:
			break;
		default:
			break;
		}
		if (data == 0x04)
			break;
		reg->writeReg(data);
		sleep(1);
		cnt++;
		if (cnt > 10)
			break;
	}
	if (data == 0x04) {
		// completed successfuly
		reg->getRegisterValue("ADJMODE");
		reg->writeReg(0x00); // open mode
		return 0;
	} else
		return -1;
}

int CameraBasicControl::setGammaValue(uint value)
{
	if (value > 255)
		return -ENODATA;
	reg->getRegisterValue("GAM_OFFSET_C");
	return reg->writeReg(value);
}

int CameraBasicControl::setYGammaControl(int mode)
{
	reg->getRegisterValue("YGM_MODE_ON");
	return reg->writeReg(mode);
}

int CameraBasicControl::setBrightness(int value)
{
	if (value > 255)
		return -ENODATA;
	reg->getRegisterValue("UIBRIGHTNESS");
	return reg->writeReg(value);
}

int CameraBasicControl::setContrast(int value)
{
	if (value > 255)
		return -ENODATA;
	reg->getRegisterValue("UICONTRAST");
	return reg->writeReg(value);
}

int CameraBasicControl::setSharpness(int value)
{
	if (value > 255)
		return -ENODATA;
	reg->getRegisterValue("UISHARPNESS");
	return reg->writeReg(value);
}

int CameraBasicControl::setATREXControl(int mode)
{
	reg->getRegisterValue("ATR_ON");
	uint data = 0;
	reg->readReg(data);
	data = data & 0xF7;
	if (mode)
		data = data | 0x08;
	return reg->writeReg(data);
}

int CameraBasicControl::setBrightnessAdj(int value)
{
	if (value > 255)
		return -ENODATA;
	reg->getRegisterValue("STRENGTH_MODE_OBLEND_RATIO");
	return reg->writeReg(value);
}

int CameraBasicControl::setATRContrastGainAdj(int value)
{
	if (value > 255)
		return -ENODATA;
	reg->getRegisterValue("STRENGTH_MODE_GAIN_RATIO");
	return reg->writeReg(value);
}

