#ifndef CAMCOMMUNICATION_H
#define CAMCOMMUNICATION_H

#include <string>
#include <linux/types.h>
#include "sc_cam.h"

struct camdrv_cam_ctrl {
	struct camdrv_cam_ctrl_msg *p_send;
	struct camdrv_cam_ctrl_msg *p_recv;
};
struct camdrv_cam_ctrl_msg {
	__u32	length;
	__u8	msg[];
};

class CamCommunication
{
public:
	enum CMDMODE {
		REGREAD = 0x01,
		REGWRITE,
		CACHEREAD,
		CACHEWRITE
	};
	CamCommunication();
	struct CmdResponse {
		int data;
		int status;
		CMDMODE mode;
	};

	int doCommand(int category, uint offset, bool read = true, uint data = 0x01);
	int getCmdValue();
	int getStatus();
protected:
	int sendCmd(camdrv_cam_ctrl *ctrl);
	int parseResponse();
private:
	int fd;
	std::string device;
	SC_CAM_CTRL_RES *camRes;
	CmdResponse response;
};

#endif // CAMCOMMUNICATION_H
