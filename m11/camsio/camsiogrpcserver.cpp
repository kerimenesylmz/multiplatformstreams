#include "camsiogrpcserver.h"

#include "debug.h"

using namespace std;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;

CamsioGrpcServer::CamsioGrpcServer()
{
	ccom = new CamCommunication();
	ccontrol = new CameraBasicControl();
}

int CamsioGrpcServer::startWait(std::string ep)
{
	ServerBuilder builder;
	builder.AddListeningPort(ep, grpc::InsecureServerCredentials());
	builder.RegisterService(this);
	std::unique_ptr<Server> server(builder.BuildAndStart());
	server->Wait();
	return 0;
}

grpc::Status CamsioGrpcServer::GetValue(grpc::ServerContext *context, const camsio::CmdQ *request, camsio::CmdInfo *response)
{
	(void)context;
	int category = request->category();
	int offset = request->offset();
	gLog("Category %d, offset %d", category, offset);
	int ret = ccom->doCommand(category, offset);
	if (ret) {
		gWarn("Error for %d category nummber command", category);
		response->set_status(ccom->getStatus());
		return grpc::Status::CANCELLED;
	}
	response->set_data(ccom->getCmdValue());
	response->set_status(ccom->getStatus());
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetValue(grpc::ServerContext *context, const camsio::CmdQ *request, camsio::CmdInfo *response)
{
	(void)context;
	int category = request->category();
	int offset = request->offset();
	int data = request->data();
	gLog("Category %d, offset %d", category, offset);
	int ret = ccom->doCommand(category, offset, false, data);
	if (ret) {
		gWarn("Error for %d category nummber command", category);
		response->set_status(ccom->getStatus());
		return grpc::Status::CANCELLED;
	}
	response->set_data(ccom->getCmdValue());
	response->set_status(ccom->getStatus());
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetWhiteBalanceMode(grpc::ServerContext *context, const camsio::WhiteBalanceQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setWhiteBalanceMode(request);
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::GetWhiteBalanceMode(grpc::ServerContext *context, const camsio::Empty *request, camsio::WhiteBalanceInfo *response)
{
	(void)context;
	(void)request;
	camsio::WhiteBalanceQ *wbq = new camsio::WhiteBalanceQ();
	int ret = ccontrol->getWhiteBalanceMode(wbq);
	response->set_allocated_wb_mode(wbq);
	response->set_allocated_info(parseErrorCode(ret));
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetATWMode(grpc::ServerContext *context, const camsio::ATWModeQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setATWMode(request->mode());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetTemperatureModeValue(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setTemperatureValue(request->value());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetUserModeOptions(grpc::ServerContext *context, const camsio::UserModeQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setUserModeWB(request->mode());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetFullMWBModeValue(grpc::ServerContext *context, const camsio::FullMWBQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setFullMWBMode(request);
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::StartWhiteBalancePreAdj(grpc::ServerContext *context, const camsio::Empty *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->startWhiteBalancePreAdj();
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetGammaValue(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setGammaValue(request->value());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetYGammaControl(grpc::ServerContext *context, const camsio::ModeQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setYGammaControl(request->mode());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetBrightness(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setBrightness(request->value());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetContrast(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setContrast(request->value());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetSharpness(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setSharpness(request->value());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetATREXControl(grpc::ServerContext *context, const camsio::ModeQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setATREXControl(request->mode());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetBrightnessAdj(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setBrightnessAdj(request->value());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

grpc::Status CamsioGrpcServer::SetATRContrastGainAdj(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response)
{
	(void)context;
	int ret = ccontrol->setATRContrastGainAdj(request->value());
	response = parseErrorCode(ret);
	return grpc::Status::OK;
}

camsio::GeneralInfo* CamsioGrpcServer::parseErrorCode(int err)
{
	camsio::GeneralInfo *info = new camsio::GeneralInfo();
	info->set_error(::camsio::GeneralInfo_ErrorCodes_NO_ERROR);
	if (err)
		info->set_error(::camsio::GeneralInfo_ErrorCodes_BAD_MESSAGE);
	return info;
}
