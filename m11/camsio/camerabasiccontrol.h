#ifndef CAMERABASICCONTROL_H
#define CAMERABASICCONTROL_H

#include "m11/camsio/camsio.pb.h"

class RegisterControl;
class CameraBasicControl
{
public:
	CameraBasicControl();
	int setWhiteBalanceMode(const camsio::WhiteBalanceQ *wbq);
	int getWhiteBalanceMode(camsio::WhiteBalanceQ *info);
	int setATWMode(camsio::ATWModeQ_ATWModes mode);
	int setTemperatureValue(int value);
	int setUserModeWB(camsio::UserModeQ_UserModes mode);
	int setFullMWBMode(const camsio::FullMWBQ *mwbq);
	int startWhiteBalancePreAdj();
	int setGammaValue(uint value);
	int setYGammaControl(int mode);
	int setBrightness(int value);
	int setContrast(int value);
	int setSharpness(int value);
	int setATREXControl(int mode);
	int setBrightnessAdj(int value);
	int setATRContrastGainAdj(int value);
private:
	RegisterControl *reg;
};

#endif // CAMERABASICCONTROL_H
