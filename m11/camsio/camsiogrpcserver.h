#ifndef CAMSIOGRPCSERVER_H
#define CAMSIOGRPCSERVER_H

#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/channel.h>
#include <grpc++/create_channel.h>
#include <grpc++/client_context.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/credentials.h>
#include <grpc++/security/server_credentials.h>

#include <m11/camsio/camsio.grpc.pb.h>
#include <m11/camsio/camsio.pb.h>
#include <m11/camsio/camcommunication.h>

#include <m11/camsio/camerabasiccontrol.h>

class CamsioGrpcServer: public camsio::CamControl::Service
{
public:
	CamsioGrpcServer();
	int startWait(std::__cxx11::string ep);
	::grpc::Status GetValue(grpc::ServerContext *context, const camsio::CmdQ *request, camsio::CmdInfo *response);
	::grpc::Status SetValue(grpc::ServerContext *context, const camsio::CmdQ *request, camsio::CmdInfo *response);
	::grpc::Status SetWhiteBalanceMode(grpc::ServerContext *context, const camsio::WhiteBalanceQ *request, camsio::GeneralInfo *response);
	::grpc::Status GetWhiteBalanceMode(grpc::ServerContext *context, const camsio::Empty *request, camsio::WhiteBalanceInfo *response);
	::grpc::Status SetATWMode(grpc::ServerContext *context, const camsio::ATWModeQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetTemperatureModeValue(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetUserModeOptions(grpc::ServerContext *context, const camsio::UserModeQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetFullMWBModeValue(grpc::ServerContext *context, const camsio::FullMWBQ *request, camsio::GeneralInfo *response);
	::grpc::Status StartWhiteBalancePreAdj(grpc::ServerContext *context, const camsio::Empty *request, camsio::GeneralInfo *response);
	::grpc::Status SetGammaValue(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetYGammaControl(grpc::ServerContext *context, const camsio::ModeQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetBrightness(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetContrast(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetSharpness(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetATREXControl(grpc::ServerContext *context, const camsio::ModeQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetBrightnessAdj(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response);
	::grpc::Status SetATRContrastGainAdj(grpc::ServerContext *context, const camsio::ValueQ *request, camsio::GeneralInfo *response);
protected:
	camsio::GeneralInfo *parseErrorCode(int err);
private:
	CamCommunication *ccom;
	CameraBasicControl *ccontrol;
};

#endif // CAMSIOGRPCSERVER_H
