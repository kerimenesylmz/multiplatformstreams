#include "registercontrol.h"

#include <iostream>
#include <dirent.h>
#include <stdlib.h>

#include "debug.h"
#include <db/leveldbops.h>
#include "camcommunication.h"

static std::list<std::string> getRegisterList(std::string path)
{
	std::list<std::string> regs;
	struct dirent *entry;
	DIR *dir = opendir(path.data());
	if (dir == NULL) {
		gWarn("Direction null");
		return regs;
	}
	while((entry = readdir(dir)) != NULL) {
		std::string d = entry->d_name;
		if (d.size() < 3)
			continue;
		regs.push_back(d);
	}
	closedir(dir);
	gLog("Register size got %d", regs.size());
	return regs;
}

class RegisterControlPriv
{
public:
	std::string category;
	std::string offset;
	std::string data;
	std::string regName;
	int cNo;
	int offNo;
};

RegisterControl::RegisterControl()
{
	priv = new RegisterControlPriv();
	ccom = new CamCommunication();
	regList = getRegisterList("/database/");
}

void RegisterControl::getRegisterValue(std::string reg)
{
	std::string path = "/database/" + reg;
	LevelDBOps ops(path);
	priv->category = ops.read("category");
	priv->offset = ops.read("offset");
	priv->data = ops.read("data");
	priv->regName = reg;

	priv->cNo = atoi(priv->category.c_str());
	priv->offNo = strtol(priv->offset.data(), NULL, 0);
	return;
}

int RegisterControl::writeReg(uint data)
{
	int ret = ccom->doCommand(priv->cNo, priv->offNo, false, data);
	return ret;
}

int RegisterControl::readReg(uint &data)
{
	int ret = ccom->doCommand(priv->cNo, priv->offNo);
	data = ccom->getCmdValue();
	return ret;
}

int RegisterControl::writeManualReg(int category, uint offset, uint data)
{
	int ret = ccom->doCommand(category, offset, false, data);
	return ret;
}

int RegisterControl::readManualReg(int category, uint offset, uint &data)
{
	int ret = ccom->doCommand(category, offset);
	data = ccom->getCmdValue();
	return ret;
}
