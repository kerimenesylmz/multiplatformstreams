#ifndef REGISTERCONTROL_H
#define REGISTERCONTROL_H

#include <list>
#include <string>

class CamCommunication;
class RegisterControlPriv;
class RegisterControl
{
public:
	RegisterControl();
	void getRegisterValue(std::string reg);
	int writeReg(uint data);
	int readReg(uint &data);
	int writeManualReg(int category, uint offset, uint data);
	int readManualReg(int category, uint offset, uint &data);
protected:
	std::list<std::string> regList;
	RegisterControlPriv *priv;
	CamCommunication *ccom;
};

#endif // REGISTERCONTROL_H
