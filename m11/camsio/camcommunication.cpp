#include "camcommunication.h"
#include "camcommunication.h"

#include <sc_cam.h>

#include <stdint.h>
#include <linux/types.h>
#include <cstring>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <debug.h>
#include <errno.h>
extern "C" {
	#include <linux/videodev2.h>
}

#define IOC_MAGIC	'c'
#define CAMIOC_CAM_CTRL	_IOWR(IOC_MAGIC,  7, struct camdrv_cam_ctrl)


static int checksum(struct camdrv_cam_ctrl_msg *msg)
{
	int lenght = msg->msg[0];
	int total = 0;
	for (int i = 0; i < lenght - 1; i++)
		total = total + msg->msg[i];
	return total;
}


CamCommunication::CamCommunication()
{
	fd = open("/dev/cam", O_RDWR);
	if (fd < 0)
		gWarn("File can't open");
	gWarn("file opened %d", fd);

	unsigned char Dat_Recv[SC_CAM_CTRL_DATA_MAXSIZE+12];
	camRes =  (SC_CAM_CTRL_RES *)&Dat_Recv;
	memset(Dat_Recv, 0, sizeof(Dat_Recv));
}

int CamCommunication::doCommand(int category, uint offset, bool read, uint data)
{
	struct camdrv_cam_ctrl ctrl;
	unsigned char Dat_Send[SC_CAM_CTRL_DATA_MAXSIZE+12];
	SC_CAM_CTRL_INFO* pCamCtrl;
	pCamCtrl = (SC_CAM_CTRL_INFO *)&Dat_Send;
	ctrl.p_send = (struct camdrv_cam_ctrl_msg *)pCamCtrl;
	ctrl.p_send->msg[0] = 0x09;
	ctrl.p_send->msg[1] = 0x01;
	ctrl.p_send->msg[2] = 0x06; // valid bytes
	if (read)
		ctrl.p_send->msg[3] = REGREAD;
	else
		ctrl.p_send->msg[3] = REGWRITE;
	ctrl.p_send->msg[4] = category;
	ctrl.p_send->msg[5] = (offset >> 8) & 0xFF;
	ctrl.p_send->msg[6] = offset & 0xFF;
	ctrl.p_send->msg[7] = data;
	ctrl.p_send->msg[8] = checksum(ctrl.p_send);
	ctrl.p_send->length = 0x09;

	ctrl.p_recv = (struct camdrv_cam_ctrl_msg *)camRes;
	sendCmd(&ctrl);
	if (response.status != 0x01)
		return -1;
	return 0;
}

int CamCommunication::parseResponse()
{
	for (int i = 0; i < camRes->length; i++)
		gLog("%d: 0x%x", i, camRes->pRcv[i]);

	if (camRes->length == 5) { // write response
		response.mode = REGWRITE;
		response.data = 0x00;
		response.status = camRes->pRcv[3];
	} else if (camRes->length == 6) { // read response
		response.mode = REGREAD;
		response.data = camRes->pRcv[4];
		response.status = camRes->pRcv[3];
	}
	return 0;
}

int CamCommunication::getStatus()
{
	return response.status;
}

int CamCommunication::getCmdValue()
{
	return response.data;
}

int CamCommunication::sendCmd(struct camdrv_cam_ctrl *ctrl)
{
	if (ioctl(fd, CAMIOC_CAM_CTRL, ctrl) == -1) {
		gWarn("CAMIOC_CAM_CTRL err no is %d", errno);
		return -ENODEV;
	}
	parseResponse();
	return 0;
}

