#include "h265element.h"

#include <errno.h>

H265Element::H265Element()
	: M11Element("hevc")
{
	ini = &param.codec.hevc.ini;
	ini->threadId = 0;
	ini->bufferFrameNum = 7;

	setThreadID(ini->threadId);
	setInputSource(ini->inputSource);
	setReleaseMode(ini->releaseMode);
}
#include <debug.h>

int H265Element::start()
{
	uint id = 0;
	SC_VENC_INI_HEVC_INFO h265Info;
	int ret = sc_venc_open_hevc_session(ini, &id, &h265Info);
	gWarn("return value is %d", ret);
	if (ret < 0)
		return -ENOSTR;
	setStreamID(id);
	return M11Element::start();
}

