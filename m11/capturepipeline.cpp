#include "capturepipeline.h"

#include "debug.h"
#include <livertspserver.h>
#include <applications/nvr/videorecorder.h>
#include "m11/m11grpcelement.h"

CapturePipeline::CapturePipeline()
	: PPipeline()
{

}

void CapturePipeline::startSimpleTestStream(std::shared_ptr<M11Element> el)
{
	if (elStart(el))
		return;
	addPipe(el);
	end();
	start();
}

void CapturePipeline::startRtspStream(std::shared_ptr<M11Element> el, int port, std::string stream)
{
	if (elStart(el))
		return;
	rtsp = std::make_shared<LiveRtspServer>(port);
	rtsp->addChannel(stream, stream);
	rtsp->startServer();
	addPipe(el);
	addPipe(rtsp);
	end();
	start();
}

void CapturePipeline::startGrpcStream(std::shared_ptr<M11Element> el)
{
	if (elStart(el))
		return;
	gWarn("creating pipeline of grpc stream...");
	grpcEl = std::make_shared<M11GrpcElement>();
	addPipe(el);
	addPipe(grpcEl);
	end();
	start();
}

StreamerIface *CapturePipeline::getIface()
{
	return grpcEl->iface;
}

void CapturePipeline::startMjpegStream(std::shared_ptr<M11Element> el, int port)
{
	if (elStart(el))
		return;
	mjpeg = std::make_shared<MjpgServerElement>("10.5.176.240", port);
	addPipe(el);
	addPipe(mjpeg);
	end();
	start();
}

void CapturePipeline::startRecordPipeline(std::shared_ptr<M11Element> el)
{
	if (elStart(el))
		return;
	//TODO: check video recored element. Changed to new pmedia library.
	rec = std::make_shared<VideoRecorder>(nullptr, "/mnt/records", "rec");
	addPipe(el);
	addPipe(rec);
	end();
	start();
}

int CapturePipeline::elStart(std::shared_ptr<M11Element> el)
{
	int err = el->start();
	if (err)
		gWarn("Error %s element in starting procedure %d", el->getType().data(), err);
	return err;
}

void CapturePipeline::pipelineOutput(const PBuffer &buf)
{
	gLog("Buf size%d", buf.size());
}
