#ifndef YUVELEMENT_H
#define YUVELEMENT_H

#include <m11/m11element.h>

class YUVElement : public M11Element
{
public:
	YUVElement();
	int start();
};

#endif // YUVELEMENT_H
