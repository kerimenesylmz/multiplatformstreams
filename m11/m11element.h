#ifndef M11ELEMENT_H
#define M11ELEMENT_H

#include <string>
#include <m11/m11common.h>
#include <pipeline/pelement.h>
#include <map>

class M11ElementPriv;
class M11Element : public PElement
{
public:
	M11Element(std::string type);
	enum ImageFormat {
		UYVY = 0x0000001B,
		UYVY_5BIT = 0x7F00008A,
		YUV422Planar = 0x00000016,
		NV16M = 0x00000018,
		GREY = 0x00000023
	};

	enum VideoSource {
		VIN = 0,
		MEM1 = 1,
		MEM2 = 2
	};

	enum VideoRange {
		BT601_FULL_RANGE,
		BT601_LIMITED_RANGE,
		BT601_SUPER_WHITE,
		BT709_FULL_RANGE,
		BT709_LIMITED_RANGE,
		BT709_SUPER_WHITE,
	};

	enum VideoEncodeProfile {
		BASE_PROFILE,
		MAIN_PROFILE,
		HIGH_PROFILE
	};

	enum VideoEncodeLevel {
		Level1 = 1,
		Level1b,
		Level1_1,
		Level1_2,
		Level1_3,
		Level2,
		Level2_1,
		Level2_2,
		Level3,
		Level3_1,
		Level3_2,
		Level4,
		Level4_1,
		Level4_2,
		Level5,
		Level5_1,
	};

	enum ImageInversion {
		NO_INVERSION,
		Y_AXIS_INVERSION,
		X_AXIS_INVERSION,
		Y_X_AXIS_INVERSION
	};

	enum ImageRotation {
		NO_ROTATION,
		CLOCKWISE_90
	};

	virtual int start();
	virtual int changeParam();
	void setStreamID(uint id);
	std::string getType();
	int printParams();

	void setFrameRate(float fps);
	float getFrameRate();
	void setImageSize(int w, int h);
	int getImageWidth();
	int getImageHeight();
	int setInImageFormat(ImageFormat f);
	int setOutImageFormat(ImageFormat f);
	int getInImageFormat();
	int getOutImageFormat();
	int setInVideoRange(VideoRange r);
	int setOutVideoRange(VideoRange r);
	int getInVideoRange();
	int getOutVideoRange();
	int setBitrate(int bitrate);
	int getBitrate();
	void setLevel(VideoEncodeLevel l);
	VideoEncodeLevel getLevel();
	void setProfile(VideoEncodeProfile profile);
	VideoEncodeProfile getProfile();
	void setInversion(ImageInversion i);
	int getInversion();
	void setRotation(ImageRotation r);
	int getRotation();
	void setImageQScale(int i);
	int getImageQScale();
	void setImageDrinum(int i);
	int getImageDrinum();

	void setInputSource(int s);
	void setThreadID(int id);
	void setReleaseMode(int m);
	void setDebug(int i);
protected:
	SC_PARAM_PARAMETER param;
	int process(int ch);

	SC_JPEG_PARAM *getJpegParam();
	SC_MPEG_PARAM *getCodecParam();
	SC_IN_IMAGE_PARAM * getImageParam();
	SC_OUT_IMAGE_PARAM *getOutImageParam();
	SC_VIDEO_FRAMERATE *getFrameRateParam();
private:
	void setMaxProfileBitrate();
	int getMaxBitrate(int level);
private:
	M11ElementPriv *p;
	std::map<VideoEncodeLevel, int> maxProfileBitrate;
	std::map<VideoEncodeLevel, int> maxBitrateForMain;
	std::map<VideoEncodeLevel, int> maxBitrateForHigh;

};

typedef std::shared_ptr<M11Element> M11ElementPtr;

#endif // M11ELEMENT_H
