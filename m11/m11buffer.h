#ifndef M11BUFFER_H
#define M11BUFFER_H

#include <pipeline/pbuffer.h>
#include <sc_lib.h>
#include <sc_cam.h>
#include <sc_video.h>
#include <sc_cap.h>
#include <sc_param.h>


class M11Buffer : public PBuffer
{
public:
	M11Buffer();
	M11Buffer(SC_VENC_ENC_DATA *ves, int eID);
	~M11Buffer();

protected:
	class M11Data : public PBuffer::Data
	{
	public:
		M11Data();
		~M11Data();
		size_t getLen() const override;
		unsigned char *getData() override;

		SC_VENC_ENC_DATA *ves;
		int id;
	};
};

#endif // M11BUFFER_H
