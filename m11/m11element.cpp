#include "m11element.h"
#include "m11buffer.h"
#include "debug.h"

#include <errno.h>
#include <cstring>

class M11ElementPriv
{
public:
	uint id;
	int timeout;
	int bufferCount;
	bool isStreaming;
	std::string type;
	int threadID;
	int inputSource;
	int releaseMode;
	int debug;
};

M11Element::M11Element(std::string type)
	: PElement()
{
	p = new M11ElementPriv();
	p->id = 0;
	p->type = type;
	p->bufferCount = 0;
	p->timeout = 10 * 1000;
	p->isStreaming = false;
	p->inputSource = 0;
	p->debug = 0;

	if (!type.empty()) {
		memset(&param, 0, sizeof(param));
		if (sc_param_read_codec(&param, type.data()))
			gWarn("Parameter uninitializing");
	}
	setMaxProfileBitrate();
}

void M11Element::setStreamID(uint id)
{
	p->id = id;
}

std::__cxx11::string M11Element::getType()
{
	return p->type;
}

int M11Element::start()
{
	if (p->debug)
		printParams();
	if (p->id != 0) {
		int err = sc_venc_start(p->id);
		if (!err) {
			gWarn("%s session is start with %d", p->type.data(), p->id);
			p->isStreaming = true;
		}
		else gWarn("Encoder stream start error %d", errno);
	} else
		gWarn("Stream id is undefined %d", p->id);
	return 0;
}

int M11Element::changeParam()
{
	gWarn("m11 element change param");
	return 0;
}

int M11Element::process(int ch)
{
	(void)ch;
	while (!shouldJoin()) {
		if (p->isStreaming) {
			SC_VENC_ENC_DATA *ves = new SC_VENC_ENC_DATA;
			int err = sc_venc_get_stream_data(p->id, p->timeout, ves);
			if (err)
				continue;
			M11Buffer buf(ves, p->id);
			buf.setTimestamp(ves->timeStamp, PBuffer::CLOCK_90KHZ);
			buf.setStreamBufferNo(p->bufferCount++);
			buf.setDuration(90000 * (double)((double)1 / (double)getFrameRate()));
			outq[ch]->enqueue(buf);
		}
	}
	return 0;
}

SC_IN_IMAGE_PARAM *M11Element::getImageParam()
{
	if (p->type == "h264")
		return &param.codec.h264.ini.inImage;
	else if (p->type == "hevc")
		return &param.codec.hevc.ini.inImage;
	else if (p->type == "jpeg")
		return &param.codec.jpeg.p.inImage;
	else if (p->type == "yuv422")
		return &param.codec.yuv.ini.inImage;
	else
		return nullptr;
}

SC_OUT_IMAGE_PARAM* M11Element::getOutImageParam()
{
	if (p->type == "h264")
		return &param.codec.h264.ini.outImage;
	else if (p->type == "hevc")
		return &param.codec.hevc.ini.outImage;
	else if (p->type == "jpeg")
		return &param.codec.jpeg.p.outImage;
	else if (p->type == "yuv422")
		return &param.codec.yuv.ini.outImage;
	else
		return nullptr;
}

SC_MPEG_PARAM* M11Element::getCodecParam()
{
	if (p->type == "h264")
		return &param.codec.h264.ini.codecParam;
	else if (p->type == "hevc")
		return &param.codec.hevc.ini.codecParam;
	else if (p->type == "jpeg")
		return nullptr;
	else if (p->type == "yuv422")
		return nullptr;
	else
		return nullptr;
}

SC_JPEG_PARAM* M11Element::getJpegParam()
{
	if (p->type == "jpeg")
		return &param.codec.jpeg.p.codecParam;
	else
		return nullptr;
}

SC_VIDEO_FRAMERATE* M11Element::getFrameRateParam()
{
	if (p->type == "h264")
		return &param.codec.h264.ini.frameRate;
	else if (p->type == "hevc")
		return &param.codec.hevc.ini.frameRate;
	else if (p->type == "jpeg")
		return &param.codec.jpeg.p.frameRate;
	else if (p->type == "yuv422")
		return &param.codec.yuv.ini.frameRate;
	else
		return nullptr;
}

int M11Element::printParams()
{
	gWarn("Image Width %d, Image Height %d", getImageWidth(), getImageHeight());
	gWarn("Stream FPS %f", getFrameRate());
	gWarn("Input Image Format %d", getInImageFormat());
	gWarn("Output Image Format %d", getOutImageFormat());
	gWarn("Input Video range %d", getInVideoRange());
	gWarn("Output Video range %d", getOutVideoRange());
	if (p->type != "jpeg") {
		gWarn("Bitrate %d", getBitrate());
		gWarn("Max bitrate %d", getMaxBitrate(getLevel()));
		gWarn("Encode level %d", getLevel());
		gWarn("Encode profile %d", getProfile());
	}
	gWarn("Invert State %d", getInversion());
	gWarn("Rotation State %d", getRotation());
	if (p->type == "jpeg") {
		gWarn("Image qScale %d", getJpegParam()->qScale);
		gWarn("Image driNum %d", getJpegParam()->driNum);
	}
	return 0;
}

void M11Element::setMaxProfileBitrate()
{
	maxProfileBitrate[Level1] = 64;
	maxProfileBitrate[Level1b] = 128;
	maxProfileBitrate[Level1_1] = 192;
	maxProfileBitrate[Level1_2] = 384;
	maxProfileBitrate[Level1_3] = 768;
	maxProfileBitrate[Level2] = 2000;
	maxProfileBitrate[Level2_1] = 4000;
	maxProfileBitrate[Level2_2] = 4000;
	maxProfileBitrate[Level3] = 10000;
	maxProfileBitrate[Level3_1] = 14000;
	maxProfileBitrate[Level3_2] = 20000;
	maxProfileBitrate[Level4] = 20000;
	maxProfileBitrate[Level4_1] = 50000;
	maxProfileBitrate[Level4_2] = 50000;
	maxProfileBitrate[Level5] = 135000;
	maxProfileBitrate[Level5_1] = 240000;
}

int M11Element::getMaxBitrate(int level)
{
	int maxBitrate = maxProfileBitrate.at(static_cast<VideoEncodeLevel>(level));
	switch (getProfile()) {
	case BASE_PROFILE:
	case MAIN_PROFILE:
		maxBitrate = maxBitrate * 1000;
		break;
	case HIGH_PROFILE:
		maxBitrate = maxBitrate * 1250;
		break;
	default:
		break;
	}
	return maxBitrate;
}

void M11Element::setFrameRate(float fps)
{
	getFrameRateParam()->frameRateNum = fps;
	getFrameRateParam()->frameRateDenom = 1;
}

float M11Element::getFrameRate()
{
	return getFrameRateParam()->frameRateNum / getFrameRateParam()->frameRateDenom;
}

void M11Element::setImageSize(int w, int h)
{
	getOutImageParam()->imageParam.width = w;
	getOutImageParam()->imageParam.height= h;
}

int M11Element::getImageWidth()
{
	return getOutImageParam()->imageParam.width;
}

int M11Element::getImageHeight()
{
	return getOutImageParam()->imageParam.height;
}

int M11Element::setInImageFormat(ImageFormat f)
{
	switch (p->inputSource) {
	case VIN:
		break;
	case MEM1:
		if ((f == UYVY) || (f == UYVY_5BIT))
			getImageParam()->imageParam.imageFormat = f;
		else
			goto ret;
		break;
	case MEM2:
		if ((f == UYVY) || (f == UYVY_5BIT))
			getImageParam()->imageParam.imageFormat = f;
		else goto ret;
		break;
	default:
		break;
	}
	return 0;
ret:
	gWarn("Check documentation for image format supports. page 80");
	return -1;
}

int M11Element::setOutImageFormat(ImageFormat f)
{
	switch (p->inputSource) {
	case VIN:
		if ((f == UYVY) || (f == UYVY_5BIT))
			getOutImageParam()->imageParam.imageFormat = f;
		else
			goto ret;
		break;
	case MEM1:
		if ((f == UYVY) || (f == UYVY_5BIT))
			getImageParam()->imageParam.imageFormat = f;
		else
			goto ret;
		break;
	case MEM2: {
		if ((f != UYVY) && (f != UYVY_5BIT))
			goto ret;
		getOutImageParam()->imageParam.imageFormat = f;
		int inF = getImageParam()->imageParam.imageFormat;
		if (inF != f)
			getImageParam()->imageParam.imageFormat = f;
		break;
	}
	default:
		break;
	}
ret:
	gWarn("Check documentation for image format supports. page 80");
	return -1;
}

int M11Element::getInImageFormat()
{
	return getImageParam()->imageParam.imageFormat;
}

int M11Element::getOutImageFormat()
{
	return getOutImageParam()->imageParam.imageFormat;
}

int M11Element::setInVideoRange(VideoRange r)
{
	switch (p->inputSource) {
	case VIN:
		break;
	case MEM1:
		if (r == BT601_FULL_RANGE)
			getImageParam()->imageParam.videoRange = r;
		else
			goto ret;
		break;
	case MEM2:
		getImageParam()->imageParam.videoRange = r;
		break;
	default:
		break;
	}
ret:
	gWarn("Check documentation for image format supports.");
	return -1;
}

int M11Element::setOutVideoRange(VideoRange r)
{
	switch (p->inputSource) {
	case VIN:
	case MEM1:
		getOutImageParam()->imageParam.videoRange = r;
		break;
	case MEM2: {
		getOutImageParam()->imageParam.videoRange = r;
		int inR = getImageParam()->imageParam.videoRange;
		if (inR != r)
			getImageParam()->imageParam.videoRange = r;
		break;
	}
	default:
		break;
	}
	return 0;
}

int M11Element::getInVideoRange()
{
	return getImageParam()->imageParam.videoRange;
}

int M11Element::getOutVideoRange()
{
	return getOutImageParam()->imageParam.videoRange;
}

int M11Element::setBitrate(int bitrate)
{
	if (bitrate > getMaxBitrate(Level5_1)) {
		gWarn("Maximum bitrate value is %d"
			  "Current profile is %d ", getMaxBitrate(Level5_1), getProfile());
		return -1;
	}
	int maxBitrate = getMaxBitrate(getLevel());
	if (bitrate > maxBitrate) {
		gWarn("Change current level or profile, Maxbitrate is %d for this %d profile and %d level", maxBitrate, getProfile(), getLevel());
		return -2;
	}
	getCodecParam()->targetBitrate = bitrate;
	return 0;
}

int M11Element::getBitrate()
{
	return getCodecParam()->targetBitrate;
}

void M11Element::setLevel(M11Element::VideoEncodeLevel l)
{
	getCodecParam()->level = l;
}

M11Element::VideoEncodeLevel M11Element::getLevel()
{
	return static_cast<VideoEncodeLevel>(getCodecParam()->level);
}

void M11Element::setProfile(VideoEncodeProfile profile)
{
	getCodecParam()->profile = profile;
}

M11Element::VideoEncodeProfile M11Element::getProfile()
{
	return static_cast<VideoEncodeProfile>(getCodecParam()->profile);
}

void M11Element::setInversion(M11Element::ImageInversion i)
{
	getOutImageParam()->invert = i;
}

int M11Element::getInversion()
{
	return getOutImageParam()->invert;
}

void M11Element::setRotation(M11Element::ImageRotation r)
{
	getOutImageParam()->rotation = r;
}

int M11Element::getRotation()
{
	return getOutImageParam()->rotation;
}

void M11Element::setImageQScale(int i)
{
	if (i > 100)
		return ;
	getJpegParam()->qScale = i;
}

int M11Element::getImageQScale()
{
	return getJpegParam()->qScale;
}

void M11Element::setImageDrinum(int i)
{
	if (i > 24575)
		return;
	getJpegParam()->driNum = i;
}

int M11Element::getImageDrinum()
{
	return getJpegParam()->driNum;
}

void M11Element::setInputSource(int s)
{
	p->inputSource = s;
}

void M11Element::setReleaseMode(int m)
{
	p->releaseMode = m;
}

void M11Element::setDebug(int i)
{
	p->debug = i;
}

void M11Element::setThreadID(int id)
{
	p->threadID = id;
}
