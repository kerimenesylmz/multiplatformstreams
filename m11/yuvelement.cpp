#include "yuvelement.h"

#include <errno.h>
#include <debug.h>

YUVElement::YUVElement()
	: M11Element("yuv422")
{
	param.codec.yuv.ini.threadId = 0;
	param.codec.yuv.ini.outImage.imageParam.frameWidth = 2048;
	param.codec.yuv.ini.outImage.imageParam.frameHeight = 1536;
	param.codec.yuv.ini.outImage.imageParam.stride = 2048 * 2;
	param.codec.yuv.ini.outImage.imageParam.sliceHeight = 1536;
}

int YUVElement::start()
{
	uint id = 0;
	SC_VENC_INI_YUV_INFO yuvInfo;
	gWarn("%d", param.codec.yuv.ini.outImage.imageParam.width);
	gWarn("%d", param.codec.yuv.ini.outImage.imageParam.frameWidth);
	gWarn("%d", param.codec.yuv.ini.outImage.imageParam.height);
	gWarn("%d", param.codec.yuv.ini.outImage.imageParam.frameHeight);
	if (sc_venc_open_yuv_session(&param.codec.yuv.ini, &id, &yuvInfo)) {
		gWarn("%d", errno);
		return -ENOSTR;
	}
	setStreamID(id);
	return M11Element::start();
}
