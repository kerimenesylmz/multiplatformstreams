HEADERS += \
    $$PWD/m11element.h \
    $$PWD/m11common.h \
    $$PWD/h264element.h \
    $$PWD/capturepipeline.h \
    $$PWD/h265element.h \
    $$PWD/jpegelement.h \
    $$PWD/snapshotelement.h \
    $$PWD/m11buffer.h \
    $$PWD/yuvelement.h \
    $$PWD/m11grpcelement.h

SOURCES += \
    $$PWD/m11element.cpp \
    $$PWD/m11common.cpp \
    $$PWD/h264element.cpp \
    $$PWD/capturepipeline.cpp \
    $$PWD/h265element.cpp \
    $$PWD/jpegelement.cpp \
    $$PWD/snapshotelement.cpp \
    $$PWD/m11buffer.cpp \
    $$PWD/yuvelement.cpp \
    $$PWD/m11grpcelement.cpp
LIBS += -ldlib
