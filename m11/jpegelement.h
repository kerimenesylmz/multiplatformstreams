#ifndef JPEGELEMENT_H
#define JPEGELEMENT_H

#include <m11/m11element.h>

class JpegElement : public M11Element
{
public:
	JpegElement();
	int start();
protected:
	SC_VENC_INI_JPEG *ini;
};

#endif // JPEGELEMENT_H
