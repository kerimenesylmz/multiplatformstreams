#ifndef CAPTUREPIPELINE_H
#define CAPTUREPIPELINE_H

#include <pipeline/ppipeline.h>
#include <m11/m11element.h>
#include <applications/mjpeg/mjpgserverelement.h>
#include <m11/m11grpcelement.h>

class VideoRecorder;
class LiveRtspServer;
class CapturePipeline : public PPipeline
{
public:
	CapturePipeline();
	void startSimpleTestStream(std::shared_ptr<M11Element> el);
	void startRtspStream(std::shared_ptr<M11Element> el, int port, std::string stream);
	void startMjpegStream(std::shared_ptr<M11Element> el, int port);
	void startRecordPipeline(std::shared_ptr<M11Element> el);
	void startGrpcStream(std::shared_ptr<M11Element> el);
	StreamerIface * getIface();
protected:
	void pipelineOutput(const PBuffer &buf);
	int elStart(std::shared_ptr<M11Element> el);
private:
	std::shared_ptr<LiveRtspServer> rtsp;
	std::shared_ptr<MjpgServerElement> mjpeg;
	std::shared_ptr<VideoRecorder> rec;
	std::shared_ptr<M11GrpcElement> grpcEl;
};

#endif // CAPTUREPIPELINE_H
