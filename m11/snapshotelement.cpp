#include "snapshotelement.h"

#include <errno.h>

SnapshotElement::SnapshotElement()
	: M11Element("jpeg")
{
	ini = &param.codec.jpeg.p;

	ini->threadId = 0x100;
	ini->bufferFrameNum = 7;

	setThreadID(ini->threadId);
	setInputSource(ini->inputSource);
	setReleaseMode(ini->releaseMode);
}

int SnapshotElement::start()
{
	id = 0;
	if (sc_venc_open_snapshot_jpeg(ini, &id) < 0)
		return -ENOSTR;
	setStreamID(id);
	return M11Element::start();
}
