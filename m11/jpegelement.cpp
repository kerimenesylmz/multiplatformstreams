#include "jpegelement.h"

#include <errno.h>

JpegElement::JpegElement()
	: M11Element("jpeg")
{
	ini = &param.codec.jpeg.p;

	ini->threadId = 0;
	ini->bufferFrameNum = 7;

	setThreadID(ini->threadId);
	setInputSource(ini->inputSource);
	setReleaseMode(ini->releaseMode);
}

int JpegElement::start()
{
	uint id = 0;
	SC_VENC_INI_JPEG_INFO jpegInfo;
	if (sc_venc_open_jpeg_session(ini, &id, &jpegInfo) < 0)
		return -ENOSTR;
	setStreamID(id);
	return M11Element::start();
}

