#ifndef SNAPSHOTELEMENT_H
#define SNAPSHOTELEMENT_H

#include <m11/m11element.h>

class SnapshotElement : public M11Element
{
public:
	SnapshotElement();
	int start();
protected:
	uint id;
	SC_VENC_INI_JPEG *ini;
};

#endif // SNAPSHOTELEMENT_H
