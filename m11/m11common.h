#ifndef M11COMMON_H
#define M11COMMON_H

#include <sc_lib.h>
#include <sc_cam.h>
#include <sc_cap.h>
#include <sc_video.h>
#include <sc_param.h>
#include <string>

class M11Common
{
public:
	M11Common();
	int videoInit();
	int paramInit();
	int videoExit();
	int paramExit();
	int captureInit();
	int captureExit();
};

#endif // M11COMMON_H
