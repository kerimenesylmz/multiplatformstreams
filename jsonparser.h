#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <string>
#include <data/json.hpp>
using json = nlohmann::json;

class JsonParser
{
public:
	JsonParser(std::__cxx11::string filename);

	int getInt(std::string name);
	bool getBool(std::string name);
	float getFloat(std::string name);
	std::string getString(std::string name);
protected:
	json getObject(json j, std::string name);
	json iterator(std::string name);
	int checkType(json j, std::string name, char *type);
	std::string getLastKey(std::string name);
private:
	std::string data;
	json jdata;
};

#endif // JSONPARSER_H
