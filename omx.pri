HEADERS += \
    $$PWD/omxvideocontrol.h

SOURCES += \
    $$PWD/omxvideocontrol.cpp

message($$OUT_PWD)

INCLUDEPATH += $$OUT_PWD/../libomx/include
LIBS += -L$$OUT_PWD/../libomx/lib/ -lomxil-client -lomxil-core -lkd -lrt
