#include "omxvideocontrol.h"
#include "debug.h"
#include <memory>
#include <cstring>
#define CP_CODEC_ALIGN(value, align) ((value+(align-1)) & ~(align-1))

OmxVideoControl::OmxVideoControl()
{

}

int OmxVideoControl::createPipeline()
{
	omxil_pipeline_attr_t attr = {65536, 2, OMXIL_PIPELINE_TUNNELED,};
	int ret = omxil_pipeline_create(&g_pipe, &attr);
	if (ret)
		gWarn("Pipeline error... %d", ret);
	return ret;
}

int OmxVideoControl::start()
{
	createPipeline();
	const char *video_capture = "OMX.sony.camera_m.full0";
	const char *video_splitter = "OMX.sony.video_splitter.binary.full01";

	int ret = omxil_pipeline_append(g_pipe, video_capture, NULL);
	gWarn("pipelline append %d", ret);

	setComponentRole(video_capture, (char*)"camera.uyvyebc5.0");
	setSensorModeParams(video_capture, 2048, 1080, 25, OMX_TRUE);
	getSensorModeParams(video_capture);

	OMX_PARAM_PORTDEFINITIONTYPE param;
	getPortDefinitionParams(video_capture, 0, &param);
	getStride(param.format.video.eColorFormat, 0, pars.width, pars.height, 16);
	pars.nSliceHeight = param.format.video.nSliceHeight;
	pars.eColorFormat = param.format.video.eColorFormat;
//	pars.eCompressionFormat = param.format.video.eCompressionFormat;
	gWarn("pars.nslice %d", pars.nSliceHeight);
	gWarn("%d", pars.eColorFormat);

	gWarn("%d", setPortDefinitionParams(video_capture, 0));
	ret = omxil_pipeline_start(g_pipe);
	gWarn("Pipeline start %d", ret);
	return 0;
}

int OmxVideoControl::setComponentRole(const char* name, const char* role)
{
	OMX_PARAM_COMPONENTROLETYPE param;
	request.type = OMXIL_ELEMENT_GET_PARAM;
	request.index = OMX_IndexParamStandardComponentRole;
	request.nbytes = sizeof(param);
	request.data = &param;

	int ret = execPipeCommand(name);
	if (ret)
		return ret;

	memset((void*)&param, 0, (unsigned int)sizeof(param));
	memcpy(param.cRole, role, (unsigned int)strlen(role));
	request.type = OMXIL_ELEMENT_SET_PARAM;

	ret = execPipeCommand(name);
	if (ret)
		return ret;
	pars.cRole = (char*)param.cRole;
	return 0;
}

int OmxVideoControl::setSensorModeParams(const char* name, unsigned long w, unsigned long h, unsigned long framerate, int oneshot)
{
	OMX_PARAM_SENSORMODETYPE param;
	request.data = &param;
	request.index = OMX_IndexParamCommonSensorMode;
	request.nbytes = sizeof(param);
	request.type = OMXIL_ELEMENT_GET_PARAM;

	int ret = execPipeCommand(name);
	if (ret)
		return ret;

	gWarn("width %d", param.sFrameSize.nWidth);
	gWarn("height %d", param.sFrameSize.nHeight);
	gWarn("framerate %d", param.nFrameRate);
	gWarn("oneshot %d", param.bOneShot);

//	param.nFrameRate = framerate;
//	param.sFrameSize.nWidth = w;
//	param.sFrameSize.nHeight = h;
	param.bOneShot = (OMX_BOOL)oneshot;
	request.type = OMXIL_ELEMENT_SET_PARAM;

	ret = execPipeCommand(name);
	if (ret)
		return ret;
	return 0;
}

int OmxVideoControl::getSensorModeParams(const char *name)
{
	OMX_PARAM_SENSORMODETYPE param;
	request.data = &param;
	request.index = OMX_IndexParamCommonSensorMode;
	request.nbytes = sizeof(param);
	request.type = OMXIL_ELEMENT_GET_PARAM;
	int ret = execPipeCommand(name);
	if (ret)
		return ret;
	pars.width = param.sFrameSize.nWidth;
	pars.height = param.sFrameSize.nHeight;
	pars.framerate = param.nFrameRate;
	return 0;
}

int OmxVideoControl::getPortDefinitionParams(const char* name, int port, OMX_PARAM_PORTDEFINITIONTYPE *param)
{
	param->nPortIndex = port;

	request.index = OMX_IndexParamPortDefinition;
	request.type = OMXIL_ELEMENT_GET_PARAM;
	request.nbytes = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
	request.data = param;
	int ret = execPipeCommand(name);
	if (ret)
		return ret;
	return 0;
}

int OmxVideoControl::getStride(int eFormat, unsigned char rotation, int width, int height, int align)
{
	int w;
	int nStrideY = 0;
	switch (eFormat) {
	case OMX_COLOR_FormatCbYCrY:
		if (rotation == 1) {
			w = CP_CODEC_ALIGN(height, 32);
			nStrideY = w * 2;
		} else {
			w = CP_CODEC_ALIGN(width, 2);
			nStrideY = CP_CODEC_ALIGN(w * 2, align);
		}
		break;
	case OMX_SONY_COLOR_FormatCbYCrY5bitcompressed:
		if (rotation == 1) {
			w = CP_CODEC_ALIGN(height, 32);
			nStrideY = w * 2 * 5 / 8;
		} else {
			w = CP_CODEC_ALIGN(width, 16);
			nStrideY = CP_CODEC_ALIGN(w * 2 * 5 / 8, align);
		}
		break;
	case OMX_COLOR_FormatYUV422Planar:
		if (rotation == 1) {
			nStrideY = CP_CODEC_ALIGN(height, 32);
		} else {
			w = CP_CODEC_ALIGN(width, 4);
			nStrideY = CP_CODEC_ALIGN(w, align);
		}
		break;
	case OMX_COLOR_FormatL8:
	case OMX_COLOR_FormatYUV422SemiPlanar:
	default:
		if (rotation == 1) {
			nStrideY = CP_CODEC_ALIGN(height, 32);
		} else {
			w = CP_CODEC_ALIGN(width, 2);
			nStrideY = CP_CODEC_ALIGN(w, align);
		}
		break;
	}
	pars.nStride = nStrideY;
	return 0;
}

int OmxVideoControl::setPortDefinitionParams(const char* name, int nport)
{
	OMX_PARAM_PORTDEFINITIONTYPE param;
	param.nPortIndex = nport;

	request.data = &param;
	request.nbytes = sizeof(param);
	request.type = OMXIL_ELEMENT_GET_PARAM;
	request.index = OMX_IndexParamPortDefinition;

	int ret = execPipeCommand(name);
	if (ret)
		return ret;

	param.format.image.nFrameWidth = pars.width;
	param.format.image.nFrameHeight = pars.height;
	param.format.image.nStride = pars.nStride;
	param.format.image.nSliceHeight = pars.nSliceHeight;
//	param.format.image.eCompressionFormat = pars.eCompressionFormat;
	param.format.image.eColorFormat = pars.eColorFormat;

	request.type = OMXIL_ELEMENT_SET_PARAM;
	ret = execPipeCommand(name);
	if (ret)
		return ret;
	return 0;
}

int OmxVideoControl::execPipeCommand(const char *name)
{
	int ret = omxil_pipeline_element_control(g_pipe, name, &request);
	if (ret) {
		gWarn("request pipeline elements '%s'' component error %d", name, ret);
		return ret;
	}
	return 0;
}
