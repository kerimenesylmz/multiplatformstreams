#include "elementsettings.h"

#include "jsonparser.h"
#include <m11/m11element.h>
#include <debug.h>
#include <string>

ElementSettings::ElementSettings()
{
	encSets = new JsonParser("encoders.json");
}

int ElementSettings::adjustEl(std::shared_ptr<M11Element> el, const char *type)
{
	std::string obj(type);
	obj += '.';
	if (type == "h264" || type == "hevc")  {
		el->setBitrate(encSets->getInt(obj + "bitrate"));
		el->setFrameRate(encSets->getFloat(obj + "framerate"));
		el->setImageSize(encSets->getInt(obj + "width"), encSets->getInt(obj + "height"));
		el->setRotation(static_cast<M11Element::ImageRotation>(encSets->getInt(obj + "rotation")));
		el->setInversion(static_cast<M11Element::ImageInversion>(encSets->getInt(obj + "invert")));
		el->setLevel(static_cast<M11Element::VideoEncodeLevel>(encSets->getInt(obj + "video_level")));
		el->setOutVideoRange(static_cast<M11Element::VideoRange>(encSets->getInt(obj + "video_range")));
		el->setInImageFormat(static_cast<M11Element::ImageFormat>(encSets->getInt(obj + "image_format")));
		el->setProfile(static_cast<M11Element::VideoEncodeProfile>(encSets->getInt(obj + "video_profile")));
	}

	if (type == "jpeg" || type == "snapshot") {
		el->setFrameRate(encSets->getFloat(obj + "framerate"));
		el->setImageSize(encSets->getInt(obj + "width"), encSets->getInt(obj + "height"));
		el->setInversion(static_cast<M11Element::ImageInversion>(encSets->getInt(obj + "invert")));
		el->setRotation(static_cast<M11Element::ImageRotation>(encSets->getInt(obj + "rotation")));
		el->setInImageFormat(static_cast<M11Element::ImageFormat>(encSets->getInt(obj + "image_format")));
		el->setImageQScale(encSets->getInt(obj + "qscale"));
		el->setImageDrinum(encSets->getInt(obj + "drinum"));
	}
	return 0;
}

