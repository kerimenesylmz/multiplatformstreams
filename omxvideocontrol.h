#ifndef OMXVIDEOCONTROL_H
#define OMXVIDEOCONTROL_H

#include "apf_log.h"
#include <sys/time.h>
#include "apf_omx_client_public.h"
#include <string>
class OmxParams {
public:
	OmxParams() {

	}
	int width;
	int height;
	int framerate;
	std::string cRole;
	int fwidth;
	int fheight;
	long nStride;
	long nSliceHeight;
	OMX_VIDEO_CODINGTYPE eCompressionFormat;
	OMX_COLOR_FORMATTYPE eColorFormat;
};

class OmxVideoControl
{
public:
	OmxVideoControl();

	int start();
protected:
	int createPipeline();
	int setComponentRole(const char *name, const char *role);
	int execPipeCommand(const char *name);
	int getSensorModeParams(const char *name);
	int setSensorModeParams(const char *name, unsigned long w, unsigned long h, unsigned long framerate, int oneshot);
	int getPortDefinitionParams(const char *name, int port, OMX_PARAM_PORTDEFINITIONTYPE *param);
	int getStride(int eFormat, unsigned char rotation, int width, int height, int align);
	int setPortDefinitionParams(const char *name, int nport);
private:
	omxil_pipeline_t g_pipe;
	omxil_pipeline_request_t request;
	OmxParams pars;
};

#endif // OMXVIDEOCONTROL_H
