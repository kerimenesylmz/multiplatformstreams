#include "grpcstreamerserver.h"

using namespace std;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;
#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/channel.h>
#include <grpc++/create_channel.h>
#include <grpc++/client_context.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/credentials.h>
#include <grpc++/security/server_credentials.h>

#include "debug.h"

GrpcStreamerServer::GrpcStreamerServer(StreamerIface *iface)
{
	this->iface = iface;
}

int GrpcStreamerServer::startAndWait()
{
	std::string ep = "0.0.0.0:6090";
	gWarn("Server starting '%s'", ep.data());
	ServerBuilder builder;
	builder.AddListeningPort(ep, grpc::InsecureServerCredentials());
	builder.RegisterService(this);
	std::unique_ptr<Server> server(builder.BuildAndStart());
	server->Wait();
	return 0;
}

grpc::Status GrpcStreamerServer::GetCameraStream(grpc::ServerContext *context, ::grpc::ServerReaderWriter<vms::StreamFrame, vms::CameraStreamQ> *stream)
{
	(void)context;
	vms::CameraStreamQ q;
	if (!stream->Read(&q))
		return Status::CANCELLED;
	gWarn("got writer");
	iface->writer = stream;
	streamWait();
	iface->writer = NULL;
	return Status::OK;
}

void GrpcStreamerServer::streamWait()
{
	std::unique_lock<PMutex> lk(lock);
	wc.wait(lk);
}
