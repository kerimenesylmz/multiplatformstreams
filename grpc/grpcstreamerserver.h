#ifndef GRPCSTREAMERSERVER_H
#define GRPCSTREAMERSERVER_H

#include <m11/m11grpcelement.h>
#include "grpc/nvr.grpc.pb.h"

class GrpcStreamerServer: public vms::NvrService::Service
{
public:
	GrpcStreamerServer(StreamerIface *iface);
	int startAndWait();
	grpc::Status GetCameraStream(grpc::ServerContext *context, ::grpc::ServerReaderWriter<vms::StreamFrame, vms::CameraStreamQ> *stream);
	void streamWait();
protected:
	StreamerIface* iface;
	PMutex lock;
	std::condition_variable wc;
};

#endif // GRPCSTREAMERSERVER_H
