QT -= gui
CONFIG -= qt

TARGET = MultiPlatformStreams
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    jsonparser.cpp \
    elementsettings.cpp

include (pmedia.pri)
include (xarina/xarina.pri)
include (m11/m11.pri)
include (m11/camsio/camsio.pri)
include (db/db.pri)
include (omx.pri)
include (grpc/grpc.pri)

HEADERS += \
    jsonparser.h \
    elementsettings.h

