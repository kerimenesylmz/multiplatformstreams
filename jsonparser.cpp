#include "jsonparser.h"

#include <utils.h>
#include <data/json.hpp>
#include <debug.h>

using json = nlohmann::json;

JsonParser::JsonParser(std::string filename)
{
	unsigned char *buf;
	size_t len;

	int err = Utils::readFile(filename, &buf, &len);
	if(err)
		gWarn("File opening error %s", filename.data());
	else {
		data = std::string((const char *)buf, len);
		delete buf;
	}
	if (data.size() > 0)
		 jdata = json::parse(data);
}

json JsonParser::getObject(json j, std::string name)
{
	return j[name.data()];
}

json JsonParser::iterator(std::__cxx11::string name)
{
	if (name.find(".") == std::string::npos) //
		return jdata;
	std::vector<std::string> list = utils::split(name, '.');
	json tmp = jdata;
	for (uint i = 0; i < list.size() - 1; i++)
		tmp = getObject(tmp, list[i]);
	return tmp;
}

int JsonParser::getInt(std::string name)
{
	return iterator(name)[getLastKey(name)].get<int>();
}

std::__cxx11::string JsonParser::getString(std::__cxx11::string name)
{
	return iterator(name)[getLastKey(name)].get<std::string>();
}

bool JsonParser::getBool(std::string name)
{
	return iterator(name)[getLastKey(name)].get<bool>();
}

float JsonParser::getFloat(std::string name)
{
	return iterator(name)[getLastKey(name)].get<float>();
}

int JsonParser::checkType(json j, std::string name, char * type)
{
	if (j[name].type_name() != type)
		return -1;
	return 0;
}

std::string JsonParser::getLastKey(std::string name)
{
	std::string value;
	if (name.find(".") != std::string::npos) {
		std::vector<std::string> list = utils::split(name, '.');
		value = list[list.size() -1];
	} else
		value = name;
	gLog("Last key is %s", value.data());
	return value;
}
