#include "v4l2operations.h"

#include <iostream>
#include <string>
#include <memory>

#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/types.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "debug.h"
#include "unordered_map"

#define VCAPIOC_S_VIN_SETTING BASE_VIDIOC_PRIVATE + 3
#define VCAPIOC_G_VIN_SETTING BASE_VIDIOC_PRIVATE + 4

V4l2Operations::V4l2Operations()
{
	fd = -1;
	nonBlockingIO = false;
}

void V4l2Operations::setFileDesc(int file)
{
	fd = file;
}

int V4l2Operations::getFileDesc()
{
	return fd;
}

void V4l2Operations::setNonBlockingIO(bool blocking)
{
	nonBlockingIO = blocking;
}

int V4l2Operations::openFile(std::string filename)
{
	if (nonBlockingIO)
		fd = open(filename.data(), O_RDWR | O_NONBLOCK, 0);
	else
		fd = open(filename.data(), O_RDWR, 0);

	if (fd == -1) {
		gWarn("Error opening device '%s'", filename.data());
		return -ENODEV;
	}
	gWarn("Device opening succesfully file description number is %d", fd);
	return 0;
}

int V4l2Operations::setInput(int index, int type)
{
	if (isFileOpen())
		return -1;
	struct v4l2_input input;
	input.index = index;
	input.type = type;

	if (ioctl(fd, VIDIOC_S_INPUT, input) == -1) {
		gWarn("Failed to set video input to %d,", index);
		return -ENODATA;
	}
	gLog("Given index number established %d", index);
	return 0;
}

int V4l2Operations::getInput(int *index)
{
	if (isFileOpen())
		return -1;
	struct v4l2_input input;
	if (ioctl(fd, VIDIOC_G_INPUT, &input) == -1) {
		gWarn("Unable to get video input");
		return -EINVAL;
	}
	*index = input.index;
	gLog("Index number is %d", index);
	return 0;
}

int V4l2Operations::getFormat(struct v4l2_format *fmt)
{
	if (isFileOpen())
		return -1;
	if (ioctl(fd, VIDIOC_G_FMT, fmt) == -1) {
		gWarn("Unable to get VIDIOC_G_FMT %d", errno);
		return -EINVAL;
	}
	return 0;
}

int V4l2Operations::setFormat(struct v4l2_format *fmt)
{
	if (isFileOpen())
		return -1;
	if (ioctl(fd, VIDIOC_S_FMT, fmt) == -1) {
		gWarn("Unable to VIDIOC_S_FMT %d", errno);
		return -EINVAL;
	}
	return 0;
}

int V4l2Operations::queryCapabilities(v4l2_capability *cap)
{
	if (isFileOpen())
		return -1;
	if (ioctl(fd, VIDIOC_QUERYCAP, cap) == -1) {
		gWarn("Error in VIDIOC_QUERYCAP %d", errno);
		return -EINVAL;
	}
	if (!(cap->capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		gWarn("Device does not support capturing");
		return -EINVAL;
	}
	if (!(cap->capabilities & V4L2_CAP_STREAMING)) {
		gWarn("Device does not support streaming");
		return -EINVAL;
	}
	gLog("Driver name: %s", cap->driver);
	gLog("Card name: %s", cap->card);
	gLog("Bus Info: %s", cap->bus_info);
	gLog("Version: %d", cap->version);
	gLog("Capabilities: "
		 "V4L2_CAP_VIDEO_CAPTURE: %s"
		 "V4L2_CAP_STREAMING: %s",
		 (cap->capabilities & V4L2_CAP_VIDEO_CAPTURE) ? "true": "false",
			(cap->capabilities & V4L2_CAP_STREAMING) ? "true": "false");
	return 0;
}

int V4l2Operations::enumInput(struct v4l2_input *input)
{
	if (isFileOpen())
		return -1;
	if (ioctl(fd, VIDIOC_ENUMINPUT, input) == -1) {
		gWarn("Unable to enumaret input %d", errno);
		return -EINVAL;
	}
	gWarn("Enum input capabilities %s", input->name);
	return 0;
}

int V4l2Operations::enumFmt(struct v4l2_fmtdesc *desc)
{
	if (isFileOpen())
		return -1;
	if (ioctl(fd, VIDIOC_ENUM_FMT, desc) == -1) {
		gWarn("end of format enumaration %d", errno);
		return -EINVAL;
	}

	gWarn("desc pix format%d    %s ", desc->pixelformat, desc->description);
	return 0;
}

int V4l2Operations::reqBuffers(struct v4l2_requestbuffers *req)
{
	if (isFileOpen())
		return -1;
	int cnt = req->count;
	if (ioctl(fd, VIDIOC_REQBUFS, req) == -1) {
		gWarn("Error in VIDIOC_REQBUFS %d", errno);
		return -ENOMEM;
	}

	if (req->count < cnt || !req->count) {
		gWarn("Error buffer size limit %d", cnt);
		return -ENOBUFS;
	}
	gWarn("requested buffer size %d", req->count);
	return 0;
}

int V4l2Operations::createUserPtr(int cnt)
{
	if (isFileOpen())
		return -1;
	if (!cnt)
		return -ENOBUFS;
	for (int i = 0; i < cnt; i++) {
		struct v4l2_buffer *buf = new struct v4l2_buffer;
		v4l2buf.push_back(buf);
		userptr.push_back(0);
		v4l2buf[i]->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		v4l2buf[i]->memory = V4L2_MEMORY_MMAP;
		v4l2buf[i]->index = i;

		if (ioctl(fd, VIDIOC_QUERYBUF, v4l2buf[i]) == -1) {
			gWarn("Error in ioctl VIDIOC_QUERYBUF.. %d", errno);
			return -ENOMEM;
		}
		userptr[i] = (char*) mmap(NULL, v4l2buf[i]->length,
							PROT_READ | PROT_WRITE,
							MAP_SHARED | MAP_ANON,
							fd,
							v4l2buf[i]->m.offset);
		if (userptr[i] == MAP_FAILED) {
			gWarn("Error while memory mapping %d", errno);
			return -ENOMEM;
		}
		v4l2buf[i]->m.userptr = (unsigned long)userptr[i];
		if (ioctl(fd, VIDIOC_QBUF, v4l2buf[i]) == -1) {
			gWarn("Error while in buffering %d", errno);
			return -ENOMEM;
		}
	}
	return 0;
}

int V4l2Operations::startStreaming()
{
	if (isFileOpen())
		return -1;
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (ioctl(fd, VIDIOC_STREAMON, &type) == -1) {
		gWarn("VIDIOC_STREAMON failed on device %d", errno);
		return -EPERM;
	}
	return 0;
}

int V4l2Operations::stopStreaming()
{
	if (isFileOpen())
		return -1;
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (ioctl(fd, VIDIOC_STREAMOFF, &type) == -1) {
		gWarn("VIDIOC_STREAMOFF failed on device with err %d", errno);
		return -EPERM;
	}
	return 0;
}

int V4l2Operations::getFrame(struct v4l2_buffer *buffer)
{
	if (ioctl(fd, VIDIOC_DQBUF, buffer) == -1) {
		if (errno != EAGAIN)
			gWarn("VIDIOC_DQBUF failed with err %d", errno);
		return -1;
	}
	gLog("Buffer index %d", buffer->index);
	return 0;
}

int V4l2Operations::putFrame(struct v4l2_buffer *buffer)
{
	if (ioctl(fd, VIDIOC_QBUF, buffer) == -1) {
		gWarn("Error in VIDIOC_QBUF %d", errno);
		return -ENODATA;
	}
	return 0;
}

void *V4l2Operations::getFrameData(int index)
{
	if (index < 0)
		return 0;
	gWarn("Got buffer index number is %d", index);
	return userptr[index];
}

struct v4l2_buffer *V4l2Operations::getBuffer(int index)
{
	if (index < 0)
		return NULL;
	struct v4l2_buffer *buf = v4l2buf[index];
	return buf;
}

int V4l2Operations::isFileOpen()
{
	if(fd == -1) {
		gWarn("Please open device node..");
		return EBADF;
	}
	return 0;
}

// Private IOCTLs
int V4l2Operations::vcapSetVinSetting(struct vcap_vin_setting *vin)
{
	if(ioctl(fd, VCAPIOC_S_VIN_SETTING, vin) == -1) {
		gWarn("Error in VCAPIOC_S_VIN_SETTING %d", errno);
		return -ENOANO;
	}
	return 0;
}

int V4l2Operations::vcapGetVinSetting(struct vcap_vin_setting *vin)
{
	struct vcap_vin_setting temp_vin;
	if(ioctl(fd, VCAPIOC_G_VIN_SETTING, &temp_vin) == -1) {
		gWarn("Error in VCAPIOC_G_VIN_SETTING %d", errno);
		return -ENOANO;
	}
	*vin = temp_vin;
	return 0;
}
