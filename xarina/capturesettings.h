#ifndef CAPTURESETTINGS_H
#define CAPTURESETTINGS_H


class CaptureSettings
{
public:
	CaptureSettings();
	int getInputSource();
	int setInputSource(int v);
};

#endif // CAPTURESETTINGS_H
