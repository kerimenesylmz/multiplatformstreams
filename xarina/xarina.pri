THIRD_PARTY=$$PWD/third-party
INCLUDEPATH += $$THIRD_PARTY/mw/include
INCLUDEPATH += $$THIRD_PARTY/app/include

LIBS += -L$$MW/lib/ -lscvideo -lscsystem -lscaudio -lsccap

LIBS += -L$$MW/libomx/

LIBS += -L$$APP/lib/ -lscparam -lXaCommon

HEADERS += \
    $$PWD/capturesettings.h \
    $$PWD/v4l2operations.h \
    $$PWD/testvideoinput.h

SOURCES += \
    $$PWD/capturesettings.cpp \
    $$PWD/v4l2operations.cpp \
    $$PWD/testvideoinput.cpp
