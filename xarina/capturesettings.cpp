#include "capturesettings.h"
#include <sc_cap.h>

CaptureSettings::CaptureSettings()
{

}

int CaptureSettings::getInputSource()
{
	int v = 0;
	int ret = sc_cap_get_input_source(&v);
	if (ret)
		return -1;
	return v;
}

int CaptureSettings::setInputSource(int v)
{
	int ret = sc_cap_set_input_source(v);
	if (ret)
		return -1;
	return 0;
}
