#ifndef TESTVIDEOINPUT_H
#define TESTVIDEOINPUT_H

#include "v4l2operations.h"

class TestVideoInput
{
public:
	TestVideoInput();
	int startProcess();
private:
	V4l2Operations *vops;
};

#endif // TESTVIDEOINPUT_H
