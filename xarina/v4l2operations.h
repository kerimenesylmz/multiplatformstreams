#ifndef V4L2OPERATIONS_H
#define V4L2OPERATIONS_H

#include <iostream>
#include <string>
#include <memory>
#include <vector>

extern "C" {
	#include <linux/videodev2.h>
}

struct vcap_vin_setting {
	__u32   width;
	__u32   height;
	__u32   finterval_nmr;
	__u32   finterval_dnm;
	__u32   interlaced;
	__u32   sync_type;
	__u32   colorimetry;
	__u32   colorrange;
	__u32   sync_polarities;
	__u32   clock_polarities;
	__u32   pix_aspect_v;
	__u32   pix_aspect_h;
};

class V4l2Operations
{
public:
	V4l2Operations();
	void setFileDesc(int file);
	int getFileDesc();
	void setNonBlockingIO(bool blocking);
	int openFile(std::string filename);
	int setInput(int index, int type);
	int getInput(int *index);

	int getFormat(struct v4l2_format *fmt);
	int setFormat(struct v4l2_format *fmt);

	int queryCapabilities(v4l2_capability *cap);
	int enumInput(v4l2_input *input);
	int reqBuffers(v4l2_requestbuffers *req);
	int createUserPtr(int cnt);
	int enumFmt(v4l2_fmtdesc *desc);
	int startStreaming();
	int stopStreaming();
	int getFrame(v4l2_buffer *buffer);
	int putFrame(v4l2_buffer *buffer);
	void *getFrameData(int index);
	v4l2_buffer *getBuffer(int index);

	int vcapSetVinSetting(vcap_vin_setting *vin);
	int vcapGetVinSetting(vcap_vin_setting *vin);
protected:
	int fd;
	bool nonBlockingIO;
	int isFileOpen();
	std::vector<struct v4l2_buffer*> v4l2buf;
	std::vector<char*> userptr;
};

#endif // V4L2OPERATIONS_H
