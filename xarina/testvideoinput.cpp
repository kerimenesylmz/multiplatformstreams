#include "testvideoinput.h"
#include <debug.h>

TestVideoInput::TestVideoInput()
{
	vops = new V4l2Operations();
	vops->setNonBlockingIO(true);
	int ret = vops->openFile("/dev/video0");
	if (ret)
		gWarn("Error in device opening %d", ret);
}

int TestVideoInput::startProcess()
{
	struct v4l2_capability cap;
	int ret = vops->queryCapabilities(&cap);

	gWarn("Process completed %d", ret);
	return 0;
}
