#ifndef ELEMENTSETTINGS_H
#define ELEMENTSETTINGS_H

#include <memory>

class JsonParser;
class M11Element;
class ElementSettings
{
public:
	ElementSettings();
	int adjustEl(std::shared_ptr<M11Element> el, const char *type);

private:
	JsonParser *encSets;
};

#endif // ELEMENTSETTINGS_H
